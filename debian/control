Source: llvmlite
Section: python
Priority: optional
Maintainer: LLVM Packaging Team <pkg-llvm-team@lists.alioth.debian.org>
Uploaders: Mo Zhou <lumin@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-all-dev,
 python3-setuptools,
 llvm-9-dev,
 zlib1g-dev,
 libedit-dev,
 python3-sphinx <!nodoc>,
 python3-sphinx-rtd-theme <!nodoc>
Standards-Version: 4.5.0
Homepage: http://llvmlite.pydata.org/
Vcs-Git: https://salsa.debian.org/pkg-llvm-team/llvmlite.git
Vcs-Browser: https://salsa.debian.org/pkg-llvm-team/llvmlite/

Package: python3-llvmlite
Architecture: any
Depends:
 ${python3:Depends},
 llvm-9,
 ${misc:Depends},
 ${shlibs:Depends}
Suggests:
 llvmlite-doc
Description: LLVM Python 3 binding for writing JIT compilers
 llvmlite uses the LLVM library for JIT (just-in-time) compilation of
 Python code into native machine instructions during runtime. Instead
 of exposing large parts of the LLVM C++ API for direct calls into the
 LLVM library, llvmlite follows a lightweight multi-layered approach.
 .
 This package contains the modules and the binding for Python 3.

Package: llvmlite-doc
Section: doc
Architecture: all
Build-Profiles: <!nodoc>
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends}
Suggests:
 python3-llvmlite
Description: LLVM Python binding for writing JIT compilers (docs)
 llvmlite uses the LLVM library for JIT (just-in-time) compilation of
 Python code into native machine instructions during runtime. Instead
 of exposing large parts of the LLVM C++ API for direct calls into the
 LLVM library, llvmlite follows a lightweight multi-layered approach.
 .
 This package contains the documentation and example code.
